/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saharat.TicTacToe;

import java.util.Scanner;
import jdk.nashorn.api.tree.BreakTree;

/**
 *
 * @author Dell
 */
public class TicTacTo {

    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };
    static int countOnes = 0, countZeros = 0;
    ;
    static char player = 'X';
    static int row, col;
    static boolean isFinish = false;
    static char winner = '-';
    static Scanner kb = new Scanner(System.in);

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println("  1 2 3 ");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " Turn");
    }

    static void Input() {
        while (true) {
            System.out.println("Please input Row Col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            //System.out.println("row: " + row + " col: " + col);
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: Table at row and col is not empty");
        }
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static boolean checkX() {
        for (int i = 0, j = 0; i < table.length; i++, j++) {
            if (table[i][j] == 0) {
                countZeros++;
            } else if (table[i][j] == 1) {
                countOnes++;
            }
        }
        if (countOnes == table.length || countZeros == table.length) {
            return true;
        }
        for (int i = table.length - 1, j = table.length - 1; i >= 0; i--, j--) {
            if (table[i][j] == 0) {
                countZeros++;
            } else if (table[i][j] == 1) {
                countOnes++;
            }
        }
        if (countOnes == table.length || countZeros == table.length) {
            return true;
        }
        return false;
    }

    static void checkDraw() {
        for (int row = 0; row < 3; ++row) {
            for (int col = 0; col < 3; ++col) {
                if (table[row][col] == player) {
                    return;
                }
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();

    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println(winner + " Win!!!");
        }
    }

    static void showBye() {
        System.out.println("Bye bye");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            Input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }
}
